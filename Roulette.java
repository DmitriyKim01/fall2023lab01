import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        RouletteWheel wheel = new RouletteWheel();
        wheel.spin();
        int balance = 1000;
        String wantToBet = "";
        int bet = 0;
        int predictNumber = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Would you like to make a bet ? (yes/no)");
        wantToBet = scan.next();
        if (wantToBet.equals("yes")){
            System.out.println("How much money would you like to bet ? Balance: " + balance);
            bet = checkIfValidBet(balance, scan.nextInt());
            balance -= bet;
            System.out.println("What number would you like to bet on?");
            predictNumber = checkIfValidPredict(scan.nextInt());
            wheel.spin();
            System.out.println("The wheel number: " + wheel.getValue() + " | your number: " + predictNumber);
            balance = checkIfWon(balance, bet, predictNumber, wheel.getValue());
            System.out.println("Balance: " + balance);
        }
        else  {
            System.out.println("Have a great day !");
        }
    }

    public static int checkIfValidBet(int balance, int bet){
        Scanner scan = new Scanner(System.in);
        while (balance < bet || bet <= 0){
            System.out.println("Enter a valid bet!");
            System.out.println("Balance: " + balance);
            System.out.print("Retry: ");
            bet = scan.nextInt();
            System.out.println();
        }
        return bet;
    }
    public static int checkIfValidPredict(int predict){
        Scanner scan = new Scanner(System.in);
        while ( predict > 36 || predict <= 0){
            System.out.println("Enter a valid number!");
            System.out.println("Max: 36");
            System.out.print("Retry: ");
            predict = scan.nextInt();
            System.out.println();
        }
        return predict;
    }

    public static int checkIfWon(int balance, int bet, int predictNumber, int actualNumber){
        Scanner scan = new Scanner(System.in);
        if (predictNumber == actualNumber){
            System.out.println("You won!");
            return balance + bet * 35;
        }
        else{
            System.out.println("You lost!");
            return balance;
        }
    }
}
